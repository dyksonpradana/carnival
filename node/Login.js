var PlayFab = require("playfab-sdk/Scripts/PlayFab/PlayFab");
var PlayFabClient = require("playfab-sdk/Scripts/PlayFab/PlayFabClient");

let user;
let title = "F301D";
// let title = "95728";
exports.LoginWithEmailAddress = async function (email,pwd) {
    PlayFab.settings.titleId = title;
    // PlayFab.settings.titleId = "95728";
    var loginRequest = {
        // Currently, you need to look up the correct format for this object in the API-docs:
        // https://api.playfab.com/documentation/Client/method/LoginWithCustomID
        TitleId: PlayFab.settings.titleId,
        Email: email,
        Password: pwd
        // Email: "dyksonpradana@gmail.com",
        // Password: "dykson@PLAYFAB"
    };

    return new Promise((resolve,reject)=>{
        PlayFabClient.LoginWithEmailAddress(loginRequest, function LoginCallback(error,result){
            if (result !== null) {
                // resolve(result);
                resolve({result : result,PlayFabClient : PlayFabClient});
            } else if (error !== null) {
                reject(error);
            }
        });
    });
}

exports.LoginWithCustomId = async function () {
    PlayFab.settings.titleId = title;
    // PlayFab.settings.titleId = "95728";
    var loginRequest = {
        // Currently, you need to look up the correct format for this object in the API-docs:
        // https://api.playfab.com/documentation/Client/method/LoginWithCustomID
        CustomId: "249232595b3abcf8b83533b8b74b045e",
        CreateAccount: false
        // TitleId: "95728"
    };

    return new Promise((resolve,reject)=>{
        PlayFabClient.LoginWithCustomID(loginRequest, function LoginCallback(error,result){
            if (result !== null) {
                resolve({result : result,PlayFabClient : PlayFabClient});
            } else if (error !== null) {
                reject(error);
            }
        });
    });
}

function LoginCallback(error, result) {
    // return new Promise((resolve,reject)=>{
        if (result !== null) {
            // user = result;
            // console.log(JSON.stringify(result));
            console.log("Congratulations, you made your first successful API call!");
            // resolve(result);
        } else if (error !== null) {
            // console.log("Something went wrong with your first API call.");
            // console.log("Here's some debug information:");
            console.log(CompileErrorReport(error));
            // reject(error);
        }
    // })
}

// This is a utility function we haven't put into the core SDK yet.  Feel free to use it.
exports.CompileErrorReport = function (error) {
    if (error == null)
        return "";
    var fullErrors = error.errorMessage;
    for (var paramName in error.errorDetails)
        for (var msgIdx in error.errorDetails[paramName])
            fullErrors += "\n" + paramName + ": " + error.errorDetails[paramName][msgIdx];
    return fullErrors;
}

function Compile(data){
    console.log(JSON.stringify(data));
}

// Kick off the acutla login call
// LoginWithEmailAddress()
// .then(response=>{
//     Compile(response)
// })
// .catch(err=>{
//     console.log(JSON.stringify(CompileErrorReport(err)))
// });
// node LoginWithEmailAddress.js | python -m json.tool