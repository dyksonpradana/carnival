var PlayFab = require("playfab-sdk/Scripts/PlayFab/PlayFab");
var PlayFabServer = require("playfab-sdk/Scripts/PlayFab/PlayFabServer");
var PlayFabClient = require("playfab-sdk/Scripts/PlayFab/PlayFabClient");

function conf(){
    PlayFab.settings.titleId = "f301d";
    PlayFab.settings.developerSecretKey = "DHM91ZJPOXB1N3UI4T4Q81QTQKBJMDR7KCB48XPM33NPQZNWRT";
    // PlayFab.settings.titleId = "95728";
    // PlayFab.settings.developerSecretKey = "NCXZER3TECHYAYW4T8ORN9ZIW9GFZR77POPWR6X4YEWRE1M1KG"
}

// exports.FightingDungeon = async function () {
async function play(){
    // PlayFab.settings.titleId = "F301D";
    conf();
    var request = {
        // Currently, you need to look up the correct format for this object in the API-docs:
        // https://api.playfab.com/documentation/Client/method/LoginWithCustomID
        CatalogVersion: "mini_game",
        TableId: "play"
    };

    // return new Promise((resolve,reject)=>{
    //     PlayFabServer.GetRandomResultTables(request,(error,result)=>{
    //         if (result !== null) {
    //             resolve(result);
    //         } else if (error !== null) {
    //             reject(error);
    //         }
    //     })
    // });

    return new Promise((resolve,reject)=>{
        PlayFabServer.EvaluateRandomResultTable(request,(error,result)=>{
            if (result !== null) {
                resolve(result);
            } else if (error !== null) {
                reject(error);
            }
        })
    });
}

function LoginCallback(error, result) {
    // return new Promise((resolve,reject)=>{
        if (result !== null) {
            // user = result;
            // console.log(JSON.stringify(result));
            console.log("Congratulations, you made your first successful API call!");
            // resolve(result);
        } else if (error !== null) {
            // console.log("Something went wrong with your first API call.");
            // console.log("Here's some debug information:");
            console.log(CompileErrorReport(error));
            // reject(error);
        }
    // })
}

// This is a utility function we haven't put into the core SDK yet.  Feel free to use it.
function CompileErrorReport(error) {
    if (error == null)
        return "";
    var fullErrors = error.errorMessage;
    for (var paramName in error.errorDetails)
        for (var msgIdx in error.errorDetails[paramName])
            fullErrors += "\n" + paramName + ": " + error.errorDetails[paramName][msgIdx];
    return fullErrors;
}

function Compile(data){
    console.log(JSON.stringify(data));
}

async function getItem(){

    var request = {
        CatalogVersion: "mini_game"
    };

    // return new Promise((resolve,reject)=>{
    return new Promise((resolve,reject)=>{
        PlayFabServer.GetCatalogItems(request,(error,result)=>{
            if (result !== null) {
                resolve(result);
            } else if (error !== null) {
                reject(error);
            }
        })
    });

}

// Kick off the acutla login call
// play()
// .then(async(response)=>{
//     const loot = await getItem();
//     const resultId = response.data.ResultItemId;
//     const catalog = loot.data.Catalog;

//     // const r = catalog.filter(obj=> obj.ItemId === resultId);
//     const result = catalog.find(obj=> obj.ItemId === resultId);
//     // console.log(result.CustomData);

//     Compile(JSON.parse(result.CustomData).score);


// })
// .catch(err=>{
//     console.log(err);
//     // console.log(JSON.stringify(CompileErrorReport(err)))
//     // Compile(err)
// });


exports.PlayGames = async function(){
    let res = {
        result : null,
        error: null,
    }
    try{
        const fight = await play();
        const loot = fight.data.ResultItemId;

        const lootsTable = await getItem();
        const catalog = lootsTable.data.Catalog;

        const result = catalog.find(obj=> obj.ItemId === loot);
        const score = JSON.parse(result.CustomData).score;
        // console.log(score);
        res.result = score;
        // console.log(res);
    } catch(e){
        res.error = e;
    }

    // console.log(res);

    return res;
}
// node PlayGame.js | python -m json.tool