var readline = require('readline');
const login = require("./Login");
const gamePlay = require("./PlayGame");

const gamePrefix = "game";

let PlayFabId;
var PlayFabClient = require("playfab-sdk/Scripts/PlayFab/PlayFabClient");

let useData;

// var indicator = "menu";
// Get process.stdin as the standard input object.
var input = process.stdin;

// Set input character encoding.
input.setEncoding('utf-8');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout  
  });

// Prompt user to input data in console.
function menu(){
    rl.resume();
    console.log("\n=================MINI GAME====================");
    console.log(`total score: ${userData[gamePrefix]!=undefined?userData[gamePrefix]:0}\n`);
    console.log("\nMAIN MENU:");
    console.log("[1] play game");
    console.log("[2] user profile");
    console.log("[3] option");
    
    console.log("\n[9] switch account");
    console.log("[99] exit");
    rl.question(">>take action? ", async function(data) {
        if(data === 'exit\n'|| data ==99){
            // Program exit.
            // console.log("User input complete, program exit.");
            console.log("\nthanks for playing the mini game...");

            setTimeout(welcome(), 3000);
            // console.clear();
            
            // welcome();
            // process.exit();
        }else if(data==2||data==3){
            console.log("\nOpss, this feature is not available");
            menu();
        }else if(data==1)
        {
            let score;
            // Print user input in console.
            if(data==1){
                score = await play();
            }else console.log('User Input Data : ' + data);

            tryAgain(score);
        }else if(data==9){
            logInForm();
        }else{
            console.log("\nwrong action");
            menu();
        }
     });
}

function tryAgain(result){
    // console.log(result,result.result,result.error!=null,result.result!=null);
    if(result.error===null){
        rl.resume();

        const exp = result.result;
        if(exp==0) console.log(`--RESULT: the dungeon is empty, acquire ${result.result}exp--`);
        else console.log(`--RESULT: acquire ${result.result}exp--`);

        updataUserStatistic(exp);
        updateUserData(exp);

        console.log("[1] retry");
        console.log("[2] menu");
        rl.question(">>take action? ", async function(data) {
            let score;
            if(data==1){
                score = await play();
                tryAgain(score);
            }else {
                // indicator = "menu";
                menu();
            }
        });
    }else{
        console.log(result.error);
        menu();
    }
}

async function play(){
    // indicator = "in game";
    console.log("\nexploring dungeon...\n");
    const score = await gamePlay.PlayGames();

    return score;
}
// menu();

// When user input data and click enter key.
// input.on('data', async function (data) {
//     let score = 0;
//     if(indicator=="menu"){
//         // User input exit.
//         if(data === 'exit\n'|| data ==2){
//             // Program exit.
//             // console.log("User input complete, program exit.");
//             console.log("\nthanks for playing the mini game");
//             process.exit();
//         }else
//         {
//             // Print user input in console.
//             if(data==1){
//                 score = await play();
//             }else console.log('User Input Data : ' + data);

//             tryAgain(score);
//         }
//     }else if(indicator=="in game"){
//         if(data==1){
//             score = await play();
//             tryAgain(score);
//         }else if(data==2){
//             indicator = "menu";
//             menu();
//         }
//     }
// });

// let result = login.LoginWithEmailAddress();

function updataUserStatistic(score){

    // const request = {
    //     Statistics : [
    //         { "StatisticName": "top score", "Value": score }
    //     ]
    // }

    // PlayFabClient.UpdatePlayerStatistics(request,function(result){
    //     // console.log(result);
    // },function(err){
    //     console.log(err);
    // })

    const request = {
        FunctionName : "SetPlayerStats",
        FunctionParameter : {
            statistics : [
                { StatisticName: "top score", Value: score }
            ]
        },
        RevisionSelection : "Live",
        GeneratePlayStreamEvent : true
    }

    PlayFabClient.ExecuteCloudScript(request,function(result){
        // console.log("berhasil",result);
    },function(err){
        // console.log("error",err);
    })
}

async function updateUserData(score){
    let newValue = userData[gamePrefix]==undefined?parseInt(score):userData[gamePrefix]+parseInt(score);
    const request = {
        FunctionName : "SetPlayerData",
        FunctionParameter : {
            gamePrefix : gamePrefix,
            lastValue : userData[gamePrefix],
            newValue : newValue,
        },
        RevisionSelection : "Live",
        GeneratePlayStreamEvent : true
    }

    new Promise((resolve,reject)=>{
        PlayFabClient.ExecuteCloudScript(request,(error,result)=>{
            if (result !== null) {
                // console.log(result);
                getUserData();
                resolve(result);
            } else if (error !== null) {
                // console.log(error);
                reject(error);
            }
        })
    })
}

async function getUserData(){
    const request = {
        PlayFabId : PlayFabId
    }

    return new Promise((resolve,reject)=>{
        PlayFabClient.GetUserReadOnlyData(request,(error,result)=>{
            if (result !== null) {
                if(result.data.Data.hasOwnProperty("userData")){
                    userData = JSON.parse(result.data.Data.userData.Value);
                }else userData = {};
                // console.log(JSON.stringify([userData,userData[gamePrefix],"data"]));
                resolve(result);
            } else if (error !== null) {
                // console.log(error);
                reject(error);
            }
        })
    });
}

function welcome(){
    console.clear();
    console.log("\n----[[[ WELCOME ]]]----\n");
    logInForm();

}

function logInForm(){

    console.log(">>>>> Login ");

    let email;
    let pwd;

    rl.stdoutMuted = false;

    rl.question("Email : ", function(data) {
        email = data;

        if(email.length>0){
            rl.stdoutMuted = true;
            rl.question("Password : ", function(password) {
                // console.log('\nPassword is ' + password);
                pwd = password;
                if(pwd.length>0){
                    log_in(email,pwd);
                    rl.stdoutMuted = false;
                }else{
                    console.log("\n\nplease insert a valid password\n");
                    logInForm();
                }
            });
        }else{
            console.log("\nplease insert a valid email\n");
            logInForm();
        }
    })

    rl._writeToOutput = function _writeToOutput(stringToWrite) {
        if(stringToWrite==="Password : "){
            rl.output.write(stringToWrite);  
        }
        else if (rl.stdoutMuted)
          rl.output.write("*");
        else
          rl.output.write(stringToWrite);
      };

}

function log_in(email,pwd){
    console.log("\n\n>>> Login process...");
    // login.LoginWithCustomId().then(response=>{
    login.LoginWithEmailAddress(email,pwd).then(response=>{
        console.log(`... Login success <<<`);
        
        PlayFabId = response.result.data.PlayFabId;
        PlayFabClient = response.PlayFabClient;
        
        getUserData().then(()=>menu());

        // menu();
        // progress(result);
    })
    .catch(err=>{
        // console.log(err);
        console.log(login.CompileErrorReport(err));
        console.log(`... Login ERROR !!! <<<\n`);
        // process.exit();
        rl.question("Press ENTER to retry ", function(data) {
            console.log(`\n\n`);
            logInForm();
        })
    });
}

function progress(data){

    console.log(JSON.stringify(data));
}

// log_in();
welcome();

// node index.js | python -m json.tool