var PlayFab = require("playfab-sdk/Scripts/PlayFab/PlayFab");
var PlayFabClient = require("playfab-sdk/Scripts/PlayFab/PlayFabClient");

let user;
async function DoExampleLoginWithCustomID() {
    PlayFab.settings.titleId = "95728";
    var loginRequest = {
        // Currently, you need to look up the correct format for this object in the API-docs:
        // https://api.playfab.com/documentation/Client/method/LoginWithCustomID
        TitleId: PlayFab.settings.titleId,
        // CustomId: "GettingStartedGuide",
        // CreateAccount: true
        Email: "dyksonpradana@hotmail.co.id",
        Password: "dykson@PLAYFAB"
    };

    // PlayFabClient.LoginWithCustomID(loginRequest, LoginCallback);
    // return new Promise((resolve,reject)=>{
    //     PlayFabClient.LoginWithCustomID(loginRequest, function LoginCallback(error,result){
    //         if (result !== null) {
    //             // resolve(console.log(JSON.stringify(result)));
    //             resolve(result);
    //         } else if (error !== null) {
    //             reject(error);
    //         }
    //     });
    // });

    return new Promise((resolve,reject)=>{
        PlayFabClient.LoginWithEmailAddress(loginRequest, function LoginCallback(error,result){
            if (result !== null) {
                // resolve(console.log(JSON.stringify(result)));
                resolve(result);
            } else if (error !== null) {
                reject(error);
            }
        });
    });
}

function LoginCallback(error, result) {
    // return new Promise((resolve,reject)=>{
        if (result !== null) {
            // user = result;
            // console.log(JSON.stringify(result));
            console.log("Congratulations, you made your first successful API call!");
            // resolve(result);
        } else if (error !== null) {
            // console.log("Something went wrong with your first API call.");
            // console.log("Here's some debug information:");
            console.log(CompileErrorReport(error));
            // reject(error);
        }
    // })
}

// This is a utility function we haven't put into the core SDK yet.  Feel free to use it.
function CompileErrorReport(error) {
    if (error == null)
        return "";
    var fullErrors = error.errorMessage;
    for (var paramName in error.errorDetails)
        for (var msgIdx in error.errorDetails[paramName])
            fullErrors += "\n" + paramName + ": " + error.errorDetails[paramName][msgIdx];
    return fullErrors;
}

function addFriend(){
    let param ={
        FriendPlayFabId:  "4402844371BAB86F"
    }
    return new Promise((resolve,reject)=>{
        PlayFabClient.AddFriend(param, function LoginCallback(error,result){
            if (result !== null) {
                console.log(JSON.stringify(result));
                resolve(result);
            } else if (error !== null) {
                // console.log(JSON.stringify(error));
                reject(error);
            }
        });
    });
}

function addFriendErrorCompailer(error){
    let a;
    // if(error.errorCode==1183){
        a = error.errorCode +" "+ error.error +". "+ error.errorMessage;
        // console.log(JSON.stringify(error));
        console.log(JSON.stringify(a));
    // }
}

function setData(){
    var param = {
            Data: {
              Class: "Fighter",
              Gender: "Female",
              Icon: "Guard 3",
              Theme: "Colorful"
            },
            Permission: "Public"
    }

    return new Promise((resolve,reject)=>{
        PlayFabClient.UpdateUserData(param, function LoginCallback(error,result){
            if (result !== null) {
                console.log(JSON.stringify(result));
                resolve(result);
            } else if (error !== null) {
                console.log(JSON.stringify(error));
                reject(error);
            }
        });
    });
}

function getUserData(){
    var param = {
        PlayFabId: "4402844371BAB86F",
        keys: [
            "Class",
            "Gender"]
    }

    return new Promise((resolve,reject)=>{
        PlayFabClient.GetUserData(param, function LoginCallback(error,result){
            if (result !== null) {
                console.log(JSON.stringify(result));
                resolve(result);
            } else if (error !== null) {
                console.log(JSON.stringify(error));
                reject(error);
            }
        });
    });
}


function Compile(data){
    // let id = JSON.stringify(data.data.PlayFabId);
    // console.log(JSON.stringify(data));
    // console.log(id);
    // setData(data.data.PlayFabId);

    // addFriend().then().catch(err=>addFriendErrorCompailer(err));
    // setData().catch(err=>JSON.stringify(err));
    getUserData().catch(err=>JSON.stringify(err));
}

// Kick off the acutla login call
DoExampleLoginWithCustomID().then(response=>{Compile(response)})
.catch(err=>console.log(JSON.stringify(err)));
// node GettingStarted.js | python -m json.tool